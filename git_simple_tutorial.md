% Simple git tutorial

This document is intended as a cheatsheet and a complement to two blog posts (in French, list items are links):

- [Gérer les versions de vos fichiers : premiers pas avec git](https://bioinfo-fr.net/git-premiers-pas)
- [Git : cloner un projet, travailler à plusieurs et gérer des branches](https://bioinfo-fr.net/git-usage-collaboratif)

It covers:

- how to install git and set up your environment
- how to create a new project locally and on a hosting platform
- how to handle the changes you make to your project's files

# Set up your environment

## Install git (Mandatory, once)

- On most distributions:

```
sudo apt-get install git
```

- On other OS or for GUI clients (which you do not really need), see [git homepage](https://git-scm.com/).

## Configure your local git (Mandatory, once)

(from anywhere in a terminal)


```
git config --global user.name 'FirstName LastName'
git config --global user.email 'whatever.email@example.com'
git config --global color.ui true # optional but makes nicer output
```

From there, your local setup of git is complete, you are ready to run other git commands.

Use these commands without the `--global` option for repository-specific configuration.

Use `git config --list` to display the values of the current configuration.

## Host your projects (Recommended)
You can use `git` for managing your projects either locally or remotely (if you have an ssh access to a distant machine). However, you may also want to make sharing them easier by hosting them on a server.

There exists several companies that provide hosting services. This typically encompasses hosting the code plus having associated wikis, bug tracking and feature requests facilities, etc.

For example, you can create an account on:

- [gitlab](https://gitlab.com): free and open-source solution; unlimited public and private repositories
- [github](https://github.com): free for public repositories only
- [bitbucket](https://bitbucket.com): free for public, limited private repositories

We suggest that you use either gitlab (prefered) or github.

### Create an account

- the first time, use the **register** link
- once your account is created, use the **sign in** link

### Set up your ssh key (Optional)

Having your ssh key registered with your hosting service is not mandatory.
It will merely save you the pain of typing your user name and passwords everytime you need to push your changes.
If you do so, the git URL for your project will change from `https://the/repository/URL/sourceProject.git` to `git@gitlab.com:yourUserName/sourceProject.git` (of course, you can still use the https one).

Source: [Gitlab and SSH keys](https://gitlab.com/help/ssh/README)

You should first determine whether you already have an available ssh key, or if you need to generate one

```
cat ~/.ssh/id_rsa.pub
```

If you see a string starting with `ssh-rsa`, you are probably ok.
Looking at the `*.pub` files in `~/.ssh/` will give you an idea of the available key pairs (the public key is the name of the private key with the `.pub` suffix). `id_rsa` is your default key pair but you can have several.

If you need to generate a new pair of keys (or if you would like to have another one), run:
```
ssh-keygen -t rsa -C "your.email@your.provider" -b 4096
```
In the process, the program will ask you the name of your key pair.

Once you have a pair of keys, upload your **public key** (the one that ends with `.pub`) into your hosting account.

Then, tell your ssh client which public key to use. Append the following to your `~/.ssh/config` file (create it if necessary):
```
# GitLab.com server
Host gitlab.com  # obviously change this if you use another hosting service
PubkeyAuthentication yes
IdentityFile ~/.ssh/yourKeyName  # the private key, i.e. without the '.pub' suffix
```

Eventually, tell git to change the `origin` URL:
```
git remote set-url origin git@gitlab.com:yourUserName/sourceProject.git
```

# Create a project

![Project creation workflow](images/workflowProjectCreation.png)

## Create a new project locally

- using a terminal, go to either
    - a new directory (if you start from scratch)
    - or to the root of your project's directory (if you already have some files)
- run `git init`
- (optional) upload your local project to your hosting platform cf. [corresponding section](#Upload-your-local-project-to-your-hosting-platform)

## Create a new project from your hosting platform

- sign in to your hosting platform (requires to have created an account, cf. [create an account](#Create-an-account))
- create a new project
- (optional) fill the description, create a simple `readme.md` and choose a license
- proceed to Clone an existing project cf. [Clone an existing project](#Clone-an-existing-project)

## Upload your local project to your hosting platform

- refer to Create a new project locally cf. [corresponding section](#Create-a-new-project-locally)
- sign in to your hosting platform (requires to have created an account, cf. [create an account](#Create-an-account))
- create a new project from your hosting platform cf. [corresponding section](#Create-a-new-project-from-your-hosting-platform)
- your new project homepage will mention the URL of your git repository (it ends in `.git`, some thing like `https://the/repository/URL/sourceProject.git`)
- `git remote rename origin old-origin`
- `git remote add origin https://the/repository/URL/sourceProject.git`
- `git push -u origin --all`
- `# git push -u origin --tags # Only necessary if you have tags; if you don't know, you probably don't have any and can safely ignore this`

## Clone an existing project

The homepage for the project you intend to clone usually displays an URL (that ends in `.git`). You will need this URL (not the URL of the HTML page about the project).

![Difference between a project's webpage URL (top) and its git URL (bottom)](images/gitlabProjectURL.png)

- go to any directory (does not need to be empty, the cloning operation will create a subdirectory for the project you are cloning)
- run `git clone <https://the/repository/URL/sourceProject.git>`
- check that there is a new directory (probably `sourceProject` for the URI above)
- running `git status` in your current location will probably **result in an error, and it is fine** (otherwise you are cloning a git project inside another git project and this is generally a bad idea)
- change to the directory that contains the project you have just cloned: `cd sourceProject`
- now running `git status` should not return an error anymore. It should even tell you that everything is fine

# Use git

## Principles

![Main git commands](images/git-workflow.png)

Keep in mind the distinction between:

- the **working directory**: this is the directory where you create/modify/delete files (or subdirectories) when working on your project. The changes that you make in your working directory are not visible by the others.
- the **index**: this is a snapshot of some of your files and directories from your *working directory*. Your index is not visible by the others.
- the **(local) head**: this is the latest validated snapshot of the local version of your project. It is publicly available to those who know where your local version is (and have access to it)
- optionally the **remote head**: this is the head of a remote (usually publicly available thanks to hosting) version of your project

When you create a new project or clone an existing one, your working directory your index and the head(s) are the same.

- if the remote project is updated, you can propagate the changes to your working directory with `git pull`
- after you have made some modifications in your working directory, you can take a snapshot of the relevant ones and copy them to your index using `git add`
- you can take multiple successive snapshots until your task is completed.
- when you are satisfied with the latest snapshot, you can validate it and update your local head using `git commit`
- if you host your project remotely, you can update the remote head using `git push`

By default, all these modifications are done on the *master branch*.
If you collaborate with other developpers or want to do things properly, consider doing a temporary fork for your task by creating a new branch, which will later be merged into the master branch.
For a more professionnal git workflow, see [gitflow principles](http://nvie.com/posts/a-successful-git-branching-model/).

## know what is going on: `git status`

`git status` compares your working directory, your index and the head.
It tells you which files from your working directory differ from your index, and whether your index is different from the head.

## keep in sync with the remote head: `git pull`

`git pull` will retrieve the latest version of the project from its remote location, and update your working directory.

### Conflict
If you have in local an unpushed commit that steps on the modifications of a retrieved commit, the pull may terminate on a _conflict_.
In such case, the human programmer has to resolve the conflict by looking to each indicated file,
searching for conflicts (they are enclosed in `>>>>>>` and `<<<<<<<`), and modifying the file so the code is valid.
The programmer will then [*add*](#add-modifications-to-the-index) these file to the snapshot, and run [`git commit`](#validate-the-latest-snapshot-from-the-index-to-the-repository).
The conflict is then solved.

## add modifications to the index: `git add`

`git add <file1> [<file2]*` makes a snapshot of the file(s) (or directories) that have been modified and adds them to your index.

The `-p` flag allows one to add only parts of a file, enabling fine-grained snapshots.

## validate the latest snapshot from the index to the repository: `git commit`

`git commit [-m 'some description']` copies the latest snapshot to the local head. If the `-m` argument is not provided, you will be asked to fill the description.

## propagate your commits to a remote repository: `git push`

`git push` propagates the local head (and its history of commits) to the remote head.

## find out what happened: `git log`

`git log` displays the history of commits (and their author, and the description)

## handle branches

A git repository has at least one branch (usually `master`), but you can create additional branches to develop or fix different features in parallel.
All commits go to the current branch.

### find the available branches

`git branch` displays the available local branches. 
In a brand new repository, only `master` exists.

### create a new branch (and switch to it)

`git checkout -b someNewBranch` creates a new branch which becomes the active branch (check with `git branch`)

**Important:** on your new branch, you will run a succession of `git add`, `git commit` and `git push`.
However, to push commits, you first need to create the branch on remote by running `git push origin myBranchName`.

### change to an existing branch

`git checkout someExistingBranch`

### merge branches

```
git checkout master
git merge someExistingBranch
git push # optional
```

### delete a branch

`git branch -d someNewBranch`

Use `-D` instead of `-d` if any non-merged changes exists in `someNewBranch`. 
These changes will be lost forever.
